clear all; close all; clc;
% multi thread
% tic
% parfor i=1:51
%     %     if i <=16
%     M=(i-1)*2+40;
%     RPS_inter_individual2(500,0,50000,1,1,M);
%     %     elseif i > 16
% %     N=(i-1)*2+40;
%     %         N=(i-17)*2+30;
% %     RPS_intra_individual(500,50000,0,1,1,M,100,1,0.5);
%     %     end
% end
% toc
% struct2cell & cell2mat
% for ii = 1:1
%     i=(ii-1)*2+30;
% %     file_stack{i}=load(sprintf('/Users/Kcore/YOONGIT/rps/RPSgame/RPS_julia/2file_stack%d.csv',i));
%     b=load(sprintf('/Users/Kcore/Desktop/data/stack_inter%d.csv',i));
% %     a=struct2cell(file_stack{i});
% %     b=cell2mat(a);
%     histogram(b,'FaceAlpha',0.5,'Edgecolor','None','binwidth',2); hold on; pause(0.1);
% end

% normalizin
MM=[]; mean_life=[];
for ii = 51:51
    M=(ii-1)*2+40;
%     M=20*ii+20;
    a1=load(sprintf('/Volumes/YD/RPS/data2/inter3/1_stack_inter%d.csv',M));
    a2=load(sprintf('/Volumes/YD/RPS/data2/inter3/2_stack_inter%d.csv',M));
    a3=load(sprintf('/Volumes/YD/RPS/data2/inter3/3_stack_inter%d.csv',M));
    %     a=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/1_stack%d.csv',M));
    %     a2=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/2_stack%d.csv',M));
    %     a3=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/3_stack%d.csv',M));
    a=[a1,a2,a3];
    ma=mean(a);
    mobility=5*10^(-M*(1/20));
    MM(ii,:)=[mobility,1/ma];
    mean_life(ii,:)=[mobility,ma];
%     MM=[MM;ii,1/ma];
    h=hist(a,max(a));
    h1=(h/sum(h));
    figure;
    %     h2=(h/sum(h))/(length(a)/500);
    bar(h1,'Facealpha',0.6); hold on;%bar(h2)
    %%%% exponential curve fitting %%%%
    kk=linspace(1,length(h),length(h));
    yy=(1/ma)*exp(-(1/ma)*kk);
    hold on; plot(kk,yy,'r'); hold on;
    
end
figure; plot(MM(:,1),MM(:,2),'o'); xlabel('mobility'); ylabel('1/mean_life')
figure; plot(mean_life(:,1),mean_life(:,2),'o'); xlabel('mobility'); ylabel('mean_life')

%%%%mobility



%count numbers at death time

% T=zeros(16,1);
% T1=zeros(16,1);
% parfor k=1:16
%     M=(k-1)*2+30;
%     a=load(sprintf('/Users/Kcore/YOONGIT/rps/RPSgame/RPS_julia/stack_inter%d.csv',M));
%     a1=load(sprintf('/Users/Kcore/YOONGIT/rps/RPSgame/RPS_julia/stack_intra%d.csv',M));
%     A=[]; A1=[];
%     for i = 1:max(a)
%         if sum(a(:)==i)~=0
%             A=[A;i,log(sum(a(:)==i))];
%         end
%     end
%     for i = 1:max(a1)
%         if sum(a1(:)==i)~=0
%             A1=[A1;i,log(sum(a1(:)==i))];
%         end
%     end
%     P=polyfit(A(:,1),A(:,2),1);
%     P1=polyfit(A1(:,1),A1(:,2),1);
%     f=polyval(P,A(:,1));
%     f1=polyval(P1,A1(:,1));
%
%     T(k)=(f(end)-f(1))/(A(end,1)-A(1,1));
%     T1(k)=(f1(end)-f1(1))/(A1(end,1)-A1(1,1));
% end
% li=linspace(30,60,16);
% li=5*10.^(-li*(1/10));
% plot(li,T,'bo'); hold on;
% plot(li,T1,'ro')