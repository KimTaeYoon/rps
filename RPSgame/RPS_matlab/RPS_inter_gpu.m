clear all
% function RPS_inter(Lsize,pre,ite,reproduction_rate,selection_rate,mobility)
Lsize=100;pre=1;ite=0;reproduction_rate=1;selection_rate=1;mobility=50;
tic
L=gpuArray(randi([1,3],Lsize,Lsize));
RR=gpuArray(sprand(Lsize,Lsize,0.05)+0==0);
Lattice=L.*RR;

Trace=gpuArray(ones(Lsize,Lsize)).*RR;
Trace(:,:,2)=gpuArray(zeros(Lsize,Lsize));
% 
M=gpuArray(5*10^(-mobility*(1/10)));
stack=gpuArray([]);
eps=M*(Lsize^2)*(1/2);
r1=gpuArray((reproduction_rate)/(reproduction_rate+selection_rate+eps));
r2=gpuArray((selection_rate)/(reproduction_rate+selection_rate+eps));
r3=gpuArray((eps)/(reproduction_rate+selection_rate+eps));

% % stack_Lattice=zeros(Lsize,Lsize,pre+ite);
for ii=1:pre+ite
%     
    for i = 1:Lsize^2
        R=gpuArray(randi([1,Lsize],1,2));
        rr=gpuArray(randi(4));
%         
        switch rr
            case 1
                A=gpuArray([1,0]);
            case 2
                A=gpuArray([-1,0]);
            case 3
                A=gpuArray([0,1]);
            case 4
                A=gpuArray([0,-1]);
        end
        C1=R(1)+A(1); C2=R(2)+A(2);
        if C1>Lsize
            C1=1;
        elseif C2>Lsize
            C2=1;
        elseif C1<1
            C1=Lsize;
        elseif C2<1
            C2=Lsize;
        end
        
        neighbor=Lattice(C1,C2);
        main=Lattice(R(1),R(2));
        neighbor_trace=Trace(C1,C2,:); main_trace=Trace(R(1),R(2),:);
        p=gpuArray(rand);
        
        
        if p < r3
            Lattice(C1,C2)=main;
            Lattice(R(1),R(2))=neighbor;
            Trace(C1,C2,:)=main_trace;
            Trace(R(1),R(2),:)=neighbor_trace;
        elseif p < r3+r1 %reproduction
            if neighbor==0 && main~=0
                Lattice(C1,C2)=main;
                Trace(C1,C2,:)=1;
            end
        elseif p < r1+r2+r3
            if neighbor~=0 && main~=0
                if neighbor-main==1
                    Lattice(C1,C2,:)=0;
%                     stack=[stack,Trace(C1,C2)];
                    Trace(C1,C2,:)=0;
                elseif neighbor-main==-2
                    Lattice(C1,C2,:)=0;
%                     stack=[stack,Trace(C1,C2)];
                    Trace(C1,C2,:)=0;
                end
            end
        end
         
    end
    Trace(:,:,1)=Trace(:,:,1)+(Trace(:,:,1)~=0)-Trace(:,:,2);
    Trace(:,:,2)=zeros(Lsize,Lsize);
%     if ii > pre
%         stack_Lattice(:,:,ii)=Lattice;
%     end
%     
end
toc
% numbering=num2str(mobility);
% file_lattice=append('stack_Lattice',numbering);
% save(sprintf('file_lattice%d',mobility),'stack_Lattice');
% file_stack=append('stack',numbering);
% save(sprintf('2file_stack%d',mobility),'stack');

% end
