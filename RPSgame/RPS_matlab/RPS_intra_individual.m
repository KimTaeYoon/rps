function RPS_intra_individual(Lsize,pre,ite,reproduction_rate,selection_rate,mobility,intra1,intra2,intra3)

L=randi([1,3],Lsize,Lsize);
RR=sprand(Lsize,Lsize,0.05)+0==0;
Lattice=L.*RR;

Trace=ones(Lsize,Lsize).*RR;
Trace(:,:,2)=zeros(Lsize,Lsize);

M=5*10^(-mobility*(1/20));
stack_intra1=[];stack_intra2=[];stack_intra3=[];
eps=M*(Lsize^2)*(1/2);
intra_sum=intra1+intra2+intra3;
r1=(reproduction_rate)/(reproduction_rate+selection_rate+eps+intra_sum);
r2=(selection_rate)/(reproduction_rate+selection_rate+eps+intra_sum);
r3=(eps)/(reproduction_rate+selection_rate+eps+intra_sum);
r4=(intra1)/(reproduction_rate+selection_rate+eps+intra_sum);
r5=(intra2)/(reproduction_rate+selection_rate+eps+intra_sum);
r6=(intra3)/(reproduction_rate+selection_rate+eps+intra_sum);
% myVideo = VideoWriter('RPS_intra.avi');
% myVideo.FrameRate = 50;
% open(myVideo)
% markersize=0.5;
% sur=zeros(3,pre+ite);
for ii=1:pre+ite
    
    for i = 1:Lsize^2
        R=randi([1,Lsize],1,2);
        rr=randi(4);
        
        switch rr
            case 1
                A=[1,0];
            case 2
                A=[-1,0];
            case 3
                A=[0,1];
            case 4
                A=[0,-1];
        end
        C1=R(1)+A(1); C2=R(2)+A(2);
        if C1>Lsize
            C1=1;
        elseif C2>Lsize
            C2=1;
        elseif C1<1
            C1=Lsize;
        elseif C2<1
            C2=Lsize;
        end
        
        neighbor=Lattice(C1,C2);
        main=Lattice(R(1),R(2));
        neighbor_trace=Trace(C1,C2,:); main_trace=Trace(R(1),R(2),:);
        p=rand;
        
        
        if p < r3 %exchange
            Lattice(C1,C2)=main;
            Lattice(R(1),R(2))=neighbor;
            Trace(C1,C2,:)=main_trace;
            Trace(R(1),R(2),:)=neighbor_trace;
        elseif p < r3+r1 %reproduction
            if neighbor==0 && main~=0
                Lattice(C1,C2)=main;
                Trace(C1,C2,:)=1;
            end
        elseif p < r1+r2+r3+r4+r5+r6 %selection
            if neighbor~=0 && main~=0
                if p < r1+r2+r3
                    if neighbor-main==1
                        if Lattice(C1,C2,:)==1
                            stack_intra1=[stack_intra1, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==2
                            stack_intra2=[stack_intra2, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==3
                            stack_intra3=[stack_intra3, Trace(C1,C2)];
                        end
                        Lattice(C1,C2,:)=0;
                        Trace(C1,C2,:)=0;
                    elseif neighbor-main==-2
                        if Lattice(C1,C2,:)==1
                            stack_intra1=[stack_intra1, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==2
                            stack_intra2=[stack_intra2, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==3
                            stack_intra3=[stack_intra3, Trace(C1,C2)];
                        end
                        Lattice(C1,C2,:)=0;
                        Trace(C1,C2,:)=0;
                    end
                elseif p < r1+r2+r3+r4
                    if main==1 && neighbor==1
                        if Lattice(C1,C2,:)==1
                            stack_intra1=[stack_intra1, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==2
                            stack_intra2=[stack_intra2, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==3
                            stack_intra3=[stack_intra3, Trace(C1,C2)];
                        end
                        Lattice(C1,C2,:)=0;
                        
                        Trace(C1,C2,:)=0;
                    end
                elseif p < r1+r2+r3+r4+r5
                    if main==2 && neighbor==2
                        if Lattice(C1,C2,:)==1
                            stack_intra1=[stack_intra1, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==2
                            stack_intra2=[stack_intra2, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==3
                            stack_intra3=[stack_intra3, Trace(C1,C2)];
                        end
                        Lattice(C1,C2,:)=0;
                        
                        Trace(C1,C2,:)=0;
                    end
                elseif p < r1+r2+r3+r4+r5+r6
                    if main==3 && neighbor==3
                        if Lattice(C1,C2,:)==1
                            stack_intra1=[stack_intra1, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==2
                            stack_intra2=[stack_intra2, Trace(C1,C2)];
                        elseif Lattice(C1,C2,:)==3
                            stack_intra3=[stack_intra3, Trace(C1,C2)];
                        end
                        Lattice(C1,C2,:)=0;
                        
                        Trace(C1,C2,:)=0;
                    end
                end
            end
        end
        
    end
    Trace(:,:,1)=Trace(:,:,1)+(Trace(:,:,1)~=0)-Trace(:,:,2);
    Trace(:,:,2)=zeros(Lsize,Lsize);
    %     L1=length(find(Lattice==1));
    %     L2=length(find(Lattice==2));
    %     L3=length(find(Lattice==3));
    %     sur(:,ii)=[L1;L2;L3];
    %     if ii>pre && mod(ii,10)==1
    %
    %         isOne=Lattice==1;
    %         isTwo=Lattice==2;
    %         isThree=Lattice==3;
    %         isZero=Lattice==0;
    %         [i1,j1]=ind2sub(size(Lattice),find(isOne));
    %         [i2,j2]=ind2sub(size(Lattice),find(isTwo));
    %         [i3,j3]=ind2sub(size(Lattice),find(isThree));
    %         [i4,j4]=ind2sub(size(Lattice),find(isZero));
    %         axes('NextPlot','add','XLim',[1,Lsize],'YLim',[1,Lsize]);
    %         axis square;
    %         plot(i1,j1,'o','MarkerFaceColor',[0.84,0.18,0.18],'MarkerEdgeColor',[0.84,0.18,0.18],'MarkerSize',markersize);
    %         plot(i2,j2,'o','MarkerFaceColor',[0.23,0.1,0.76],'MarkerEdgeColor',[0.23,0.1,0.76],'MarkerSize',markersize);
    %         plot(i3,j3,'o','MarkerFaceColor',[0.86,0.71,0.09],'MarkerEdgeColor',[0.86,0.71,0.09],'MarkerSize',markersize);
    %         plot(i4,j4,'o','MarkerFaceColor',[1,1,1],'MarkerEdgeColor',[1,1,1],'MarkerSize',markersize);
    %         %         hold on;
    %         pause(.1)
    %
    %         set(gcf,'position',[0,0,Lsize,Lsize])
    %         frame = getframe(gcf);
    %         writeVideo(myVideo, frame);
    %     end
end
% figure;
% plot(1:ii,sur(:,1:ii));

csvwrite(sprintf('1_stack_intra%d.csv',mobility),stack_intra1);
csvwrite(sprintf('2_stack_intra%d.csv',mobility),stack_intra2);
csvwrite(sprintf('3_stack_intra%d.csv',mobility),stack_intra3);
% close(myVideo)
end
