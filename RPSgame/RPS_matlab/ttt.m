% RPS_inter_individual(500,0,10,1,1,100);

Lsize=500; pre=0; ite=10; reproduction_rate=1;selection_rate=1;mobility=100;
L=randi([1,3],Lsize,Lsize);
RR=sprand(Lsize,Lsize,0.05)+0==0;
Lattice=L.*RR;

Trace=ones(Lsize,Lsize).*RR;
Trace_time=zeros(Lsize,Lsize);

M=5*10^(-mobility*(1/20));
stack1=[];stack2=[];stack3=[];
eps=M*(Lsize^2)*(1/2);
r1=(reproduction_rate)/(reproduction_rate+selection_rate+eps);
r2=(selection_rate)/(reproduction_rate+selection_rate+eps);
r3=(eps)/(reproduction_rate+selection_rate+eps);
% stack_Lattice=zeros(Lsize,Lsize,pre+ite);
for ii=1:pre+ite
    
    for i = 1:Lsize^2
        R=randi([1,Lsize],1,2);
        rr=randi(4);
        
        switch rr
            case 1
                A=[1,0];
            case 2
                A=[-1,0];
            case 3
                A=[0,1];
            case 4
                A=[0,-1];
        end
        C1=R(1)+A(1); C2=R(2)+A(2);
        if C1>Lsize
            C1=1;
        elseif C2>Lsize
            C2=1;
        elseif C1<1
            C1=Lsize;
        elseif C2<1
            C2=Lsize;
        end
        
        neighbor=Lattice(C1,C2);
        main=Lattice(R(1),R(2));
        neighbor_trace=Trace(C1,C2); 
        main_trace=Trace(R(1),R(2));
        neighbor_trace_time=Trace_time(C1,C2); 
        main_trace_time=Trace_time(R(1),R(2));
        p=rand;
        
        
        if p < r3 %move
            Lattice(C1,C2)=main;
            Lattice(R(1),R(2))=neighbor;
            Trace(C1,C2)=main_trace;
            Trace(R(1),R(2))=neighbor_trace;
            Trace_time(C1,C2)=main_trace_time;
            Trace_time(R(1),R(2))=neighbor_trace_time;

        elseif p < r3+r1 %reproduction
            if neighbor==0 && main~=0
                Lattice(C1,C2)=main;
                Trace(C1,C2)=1; 
                Trace_time(C1,C2)=1; 
            end
        elseif p < r1+r2+r3 %selection
            if neighbor~=0 && main~=0
                if neighbor-main==1
                    if ii > pre
                        if Lattice(C1,C2)==1
                            stack1=[stack1; Trace(C1,C2),C1,C2];
                        elseif Lattice(C1,C2)==2
                            stack2=[stack2; Trace(C1,C2),C1,C2];
                        elseif Lattice(C1,C2)==3
                            stack3=[stack3; Trace(C1,C2),C1,C2];
                        end
                    end
                    Lattice(C1,C2)=0;
                    Trace(C1,C2)=0;
                    Trace_time(C1,C2)=0;
                    
                elseif neighbor-main==-2
                    if ii>pre
                        if Lattice(C1,C2)==1
                            stack1=[stack1; Trace(C1,C2),C1,C2];
                        elseif Lattice(C1,C2)==2
                            stack2=[stack2; Trace(C1,C2),C1,C2];
                        elseif Lattice(C1,C2)==3
                            stack3=[stack3; Trace(C1,C2),C1,C2];
                        end
                    end
                    Lattice(C1,C2)=0;
                    Trace(C1,C2)=0;
                    Trace_time(C1,C2)=0;
                end
            end
        end
        
    end
    Trace=Trace+(Trace~=0)-Trace_time;
    Trace_time=zeros(Lsize,Lsize);
    
    
    %     if ii > pre
    %         stack_Lattice(:,:,ii)=Lattice;
    %     end
    %
end