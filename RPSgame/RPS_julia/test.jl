# # # using CSV, DataFrames, DelimitedFiles, JLD

# # # # for i in 1:100
# # # #     io=open("append_test.csv", "a")
# # # #     writedlm(io, i)
# # # #     close(io)
# # # # end
# # # # readdlm("append_test.csv")

# # # a=3; b=0;
# # # if a !== 0 && b !==0
# # #     print("ok")
# # # else
# # #     a=0;
# # # end

# # # if a == 0 && b ==0
# # #     print("ok")
# # # else
# # #     a=0;
# # # end
using SparseArrays, Plots, JLD, Printf, Base.Threads, DelimitedFiles, TickTock
# function RPS_inter(Lsize, pre, ite, mobility, reproduction_rate, selection_rate)
Lsize=10;
pre=0;
ite=10;
mobility=50;
reproduction_rate=1;
selection_rate=1;
    #spreda species in lattice randomly
    S = rand(1:3, Lsize, Lsize);
    SS = sprand(Lsize, Lsize, 0.9) .> 0;
    Lattice = S .* SS;

    #tracking lifespan of individuals on lattice
    Trace = zeros(Lsize, Lsize, 2);
    Trace[:,:,1] = ones(Lsize, Lsize) .* SS;

    stack = Vector{Float64}();
    # s_Lattice = zeros(Lsize, Lsize, pre + ite);

    M = 5*10^(-mobility*(1/10));
    eps = M * (Lsize^2) * (1 / 2);

    r1 = (reproduction_rate) / (reproduction_rate + selection_rate + eps);
    r2 = (selection_rate) / (reproduction_rate + selection_rate + eps);
    r3 = (eps) / (reproduction_rate + selection_rate + eps);
    #a generation
    for ii in 1:pre + ite
        #a step(interation)
        for i in 1:Lsize^2

            R = rand(1:Lsize, 1, 2);
            c = rand(1:4);

            if c == 1
                L = [1,0];
            elseif c == 2
                L = [-1,0];
            elseif c == 3
                L = [0,1];
            else
                L = [0,-1];
            end

            N1 = R[1] + L[1]; N2 = R[2] + L[2];

            if N1 > Lsize
                N1 = 1;
            elseif N2 > Lsize
                N2 = 1;
            elseif N1 < 1
                N1 = Lsize;
            elseif N2 < 1
                N2 = Lsize;
            end
            neighbor = Lattice[N1,N2];
            host = Lattice[R[1],R[2]];
            n_trace = Trace[N1,N2,:];
            h_trace = Trace[R[1],R[2],:];
            p = rand();

            if p < r3
                Lattice[N1,N2] = host;
                Lattice[R[1],R[2]] = neighbor;
                Trace[N1,N2,:] .= h_trace;
                Trace[R[1],R[2],:] .= n_trace;
            elseif p < r1 + r3
                if neighbor == 0 && host !== 0
                    Lattice[N1,N2] = host;
                    Trace[N1,N2,:] .= 1;
                end
            elseif p < r1 + r2 + r3
                if neighbor !== 0 && host !== 0
                    if neighbor - host == 1
                        Lattice[N1,N2] = 0;
                        append!(stack, Trace[N1,N2,1]);
                        Trace[N1,N2,:] .= 0;
                    elseif neighbor - host == -2
                        Lattice[N1,N2] = 0;
                        append!(stack, Trace[N1,N2,1]);
                        Trace[N1,N2,:] .= 0;
                    end
                end
            end

        end
        Trace[:,:,1] = Trace[:,:,1] + (Trace[:,:,1] .> 0.00001) - Trace[:,:,2];
        Trace[:,:,2] = zeros(Lsize, Lsize);
        # if 
        # if ii > pre
        #     s_Lattice[:,:,ii] = Lattice;
        # end

    end
    writedlm("append1.csv",stack,',')



    # anim = @animate for k = 1 + pre:pre + ite
    #     heatmap(s_Lattice[:,:,k], color=:Blues)
    # end

###MAC
    # numb=string(mobility);
    # file1 = @sprintf "/Users/tyoon/Desktop/Git/rps/RPSgame/RPS_ani_inter%s.avi" numb
    # file2 = @sprintf "/Users/tyoon/Desktop/Git/rps/RPSgame/stack%s.jld" numb
    # gif(anim,file1, fps=100)
    # save(file2,"stack",stack)
###Window
    # numb=string(mobility);
    # file1 = @sprintf "/Users/Kcore/YOONGIT/rps/RPSgame/RPS_ani_inter%s.avi" numb
    # file2 = @sprintf "/Users/Kcore/YOONGIT/rps/RPSgame/Rps_julia/stack_inter%s.csv" numb
    # gif(anim,file1, fps=25)
    # save(file2,"stack",stack)
    # writedlm(file2,stack,',')
# end
# tick()
# RPS_inter(500,100000,0,34,1,1)
# tock()
# tick()
# Threads.@threads for i in 1:16
#     M=(i-1)*2+30
#     RPS_inter(500,300,0,M,1,1);
# end
# tock()

# for i in 1:10
#     p=rand(2,1)
#     print(p)
# end
