clear all; close all; clc;
%%%%%%%%%%%%%%intraspecific%%%%%%%%%%%%%%%
% tic
% li=[25,52,100];
% parfor kk=1:3
%     k=li(kk);
%     MM=[]; mean_life=[];
%     for ii = 1:51
%         M=(ii-1)*2+40;
%         %     M=20*ii+20;
%         a1=load(sprintf('/Users/tyoon/Desktop/RPS/intra_%d/1_stack_intra%d.csv',k,M));
%         a2=load(sprintf('/Users/tyoon/Desktop/RPS/intra_%d/2_stack_intra%d.csv',k,M));
%         a3=load(sprintf('/Users/tyoon/Desktop/RPS/intra_%d/3_stack_intra%d.csv',k,M));
%         %     a=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/1_stack%d.csv',M));
%         %     a2=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/2_stack%d.csv',M));
%         %     a3=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/3_stack%d.csv',M));
%         ma1=mean(a1);ma2=mean(a2);ma3=mean(a3);
%         mobility=5*10^(-M*(1/20));
%         MM(ii,:)=[mobility,1/ma1,1/ma2,1/ma3];
%         mean_life(ii,:)=[mobility,ma1,ma2,ma3];
%         %     MM=[MM;ii,1/ma];
%         %     h=hist(a,max(a));
%         %     h1=(h/sum(h));
%         %     figure;
%         %     h2=(h/sum(h))/(length(a)/500);
%         %     bar(h1,'Facealpha',0.6); hold on;%bar(h2)
%         %%%% exponential curve fitting %%%%
%         %     kk=linspace(1,length(h),length(h));
%         %     yy=(1/ma)*exp(-(1/ma)*kk);Q
%         %     hold on; plot(kk,yy,'r'); hold on;
%
%
%         %     l=linspace(1,max(a),max(a));
%         %     y=1*exp(-1*l);
%         %     plot(l,y)
%
%     end
%     csvwrite(sprintf('inverse_mean%d.csv',k),MM);
%     csvwrite(sprintf('mean_life%d.csv',k),mean_life);
% end
% toc
%%%%%%%%%%%%%interspecific%%%%%%%%%%%%%%%%%
tic
MM=[]; mean_life=[];
for ii = 1:51
    M=(ii-1)*2+40;
    %     M=20*ii+20;
    a1=load(sprintf('E:/RPS/data2/inter2/1_stack_inter%d.csv',M));
    a2=load(sprintf('E:/RPS/data2/inter2/2_stack_inter%d.csv',M));
    a3=load(sprintf('E:/RPS/data2/inter2/3_stack_inter%d.csv',M));
    %     a=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/1_stack%d.csv',M));
    %     a2=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/2_stack%d.csv',M));
    %     a3=load(sprintf('/Users/Kcore/YOONGIT_RPS/rps/RPSgame/RPS_matlab/3_stack%d.csv',M));
    ma1=mean(a1(:,1));ma2=mean(a2(:,1));ma3=mean(a3(:,1));
    mobility=5*10^(-M*(1/20));
    MM(ii,:)=[mobility,1/ma1,1/ma2,1/ma3];
    mean_life(ii,:)=[mobility,ma1,ma2,ma3];
    %     MM=[MM;ii,1/ma];
    %     h=hist(a,max(a));
    %     h1=(h/sum(h));
    %     figure;
    %     h2=(h/sum(h))/(length(a)/500);
    %     bar(h1,'Facealpha',0.6); hold on;%bar(h2)
    %%%% exponential curve fitting %%%%
    %     kk=linspace(1,length(h),length(h));
    %     yy=(1/ma)*exp(-(1/ma)*kk);Q
    %     hold on; plot(kk,yy,'r'); hold on;
    
    
    %     l=linspace(1,max(a),max(a));
    %     y=1*exp(-1*l);
    %     plot(l,y)
    
end
csvwrite('E:/RPS/data2/inverse_mean_inter.csv',MM);
csvwrite('E:/RPS/data2/mean_life_inter.csv',mean_life);

toc