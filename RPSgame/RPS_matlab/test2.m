close all; clear all; clc

Lsize=100;

A=randi([1,4],Lsize,Lsize);
B=zeros(Lsize+2,1);
C=zeros(1,Lsize);
AC=[C;A;C];
Lattice=[B,AC,B];

figure;

myVideo = VideoWriter('stable6.avi');
myVideo.FrameRate = 50;
open(myVideo)

for ii=1:300
    for i = 2:Lsize+1
        for j =2:Lsize+1
            A=move;
            if Lattice(i+A(1),j+A(2))-Lattice(i,j)==1
                Lattice(i+A(1),j+A(2))=Lattice(i,j);
            elseif Lattice(i+A(1),j+A(2))-Lattice(i,j)==-2
                Lattice(i+A(1),j+A(2))=Lattice(i,j);
            end
        end
    end
    isOne=Lattice==1;
    isTwo=Lattice==2;
    isThree=Lattice==3;
    [i1,j1]=ind2sub(size(Lattice),find(isOne));
    [i2,j2]=ind2sub(size(Lattice),find(isTwo));
    [i3,j3]=ind2sub(size(Lattice),find(isThree));
    axes('NextPlot','add','XLim',[1,Lsize],'YLim',[1,Lsize]);
    axis square;
    plot(i1,j1,'o','MarkerFaceColor',[0.84,0.18,0.18],'MarkerEdgeColor',[0.84,0.18,0.18],'MarkerSize',2);
    plot(i2,j2,'o','MarkerFaceColor',[0.23,0.1,0.76],'MarkerEdgeColor',[0.23,0.1,0.76],'MarkerSize',2);
    plot(i3,j3,'o','MarkerFaceColor',[0.86,0.71,0.09],'MarkerEdgeColor',[0.86,0.71,0.09],'MarkerSize',2);
    hold on;
    pause(.01)
    
    frame = getframe(gcf);
    writeVideo(myVideo, frame);
end      
close(myVideo)