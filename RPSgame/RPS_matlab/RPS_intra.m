function RPS_intra(Lsize,pre,ite,reproduction_rate,selection_rate,mobility,intra1,intra2,intra3)

L=randi([1,3],Lsize,Lsize);
RR=sprand(Lsize,Lsize,0.05)+0==0;
Lattice=L.*RR;

Trace=ones(Lsize,Lsize).*RR;
Trace_time=zeros(Lsize,Lsize);

M=5*10^(-mobility*(1/20));


eps=M*(Lsize^2)*(1/2);
intra_sum=intra1+intra2+intra3;

r1=(reproduction_rate)/(reproduction_rate+selection_rate+eps+intra_sum);
r2=(selection_rate)/(reproduction_rate+selection_rate+eps+intra_sum);
r3=(eps)/(reproduction_rate+selection_rate+eps+intra_sum);
r4=(intra1)/(reproduction_rate+selection_rate+eps+intra_sum);
r5=(intra2)/(reproduction_rate+selection_rate+eps+intra_sum);
r6=(intra3)/(reproduction_rate+selection_rate+eps+intra_sum);

myVideo = VideoWriter('RPS_intra.avi');
myVideo.FrameRate = 50;
open(myVideo)
markersize=0.5;

sur=zeros(3,pre+ite);

A=[1,0;-1,0;0,1;0, -1];

for ii=1:pre+ite
    stack_intra=[];
    R=randi([1,Lsize],Lsize^2,2);
    rr=randi([1,4],Lsize^2,1);
    p=rand(Lsize^2,1);
    
    for i = 1:Lsize^2
        
        C1=R(i,1)+A(rr(i),1); C2=R(i,2)+A(rr(i),2);
        
        if C1>Lsize
            C1=1;
        elseif C2>Lsize
            C2=1;
        elseif C1<1
            C1=Lsize;
        elseif C2<1
            C2=Lsize;
        end
        
        neighbor=Lattice(C1,C2);
        main=Lattice(R(i,1),R(i,2));
        neighbor_trace=Trace(C1,C2);
        main_trace=Trace(R(i,1),R(i,2));
        neighbor_trace_time=Trace_time(C1,C2);
        main_trace_time=Trace_time(R(i,1),R(i,2));
        
        if p(i) < r3 %exchange
            Lattice(C1,C2)=main;
            Lattice(R(i,1),R(i,2))=neighbor;
            Trace(C1,C2)=main_trace;
            Trace(R(i,1),R(i,2))=neighbor_trace;
            Trace_time(C1,C2)=main_trace_time;
            Trace_time(R(i,1),R(i,2))=neighbor_trace_time;
        elseif p(i) < r3+r1 %reproduction
            if neighbor==0 && main~=0
                Lattice(C1,C2)=main;
                Trace(C1,C2)=1;
                Trace_time(C1,C2)=1;
            end
        elseif p(i) < r1+r2+r3+r4+r5+r6 %selection
            if neighbor~=0 && main~=0
                if p(i) < r1+r2+r3
                    if neighbor-main==1
                        Lattice(C1,C2)=0;
                        if ii > pre
                            stack_intra(end+1,:)=Trace(C1,C2);
                        end
                        Trace(C1,C2)=0;
                        Trace_time(C1,C2)=0;
                    elseif neighbor-main==-2
                        Lattice(C1,C2)=0;
                        if ii > pre
                            stack_intra(end+1,:)=Trace(C1,C2);
                        end
                        Trace(C1,C2)=0;
                        Trace_time(C1,C2)=0;
                    end
                elseif p(i) < r1+r2+r3+r4
                    if main==1 && neighbor==1
                        Lattice(C1,C2)=0;
                        stack_intra(end+1,:)=Trace(C1,C2);
                        Trace(C1,C2)=0;
                        Trace_time(C1,C2)=0;
                    end
                elseif p(i) < r1+r2+r3+r4+r5
                    if main==2 && neighbor==2
                        Lattice(C1,C2)=0;
                        stack_intra(end+1,:)=Trace(C1,C2);
                        Trace(C1,C2)=0;
                        Trace_time(C1,C2)=0;
                    end
                elseif p(i) < r1+r2+r3+r4+r5+r6
                    if main==3 && neighbor==3
                        Lattice(C1,C2)=0;
                        stack_intra(end+1,:)=Trace(C1,C2);
                        Trace(C1,C2)=0;
                        Trace_time(C1,C2)=0;
                    end
                end
            end
        end
        
    end
    Trace=Trace+(Trace~=0)-Trace_time;
    Trace_time=zeros(Lsize,Lsize);
    L1=length(find(Lattice==1));
    L2=length(find(Lattice==2));
    L3=length(find(Lattice==3));
    %     sur(:,ii)=[L1;L2;L3];
    if ii>pre && mod(ii,10)==1
        sur(:,ii)=[L1;L2;L3];
        isOne=Lattice==1;
        isTwo=Lattice==2;
        isThree=Lattice==3;
        isZero=Lattice==0;
        [i1,j1]=ind2sub(size(Lattice),find(isOne));
        [i2,j2]=ind2sub(size(Lattice),find(isTwo));
        [i3,j3]=ind2sub(size(Lattice),find(isThree));
        [i4,j4]=ind2sub(size(Lattice),find(isZero));
        axes('NextPlot','add','XLim',[1,Lsize],'YLim',[1,Lsize]);
        axis square;
        plot(i1,j1,'o','MarkerFaceColor',[0.84,0.18,0.18],'MarkerEdgeColor',[0.84,0.18,0.18],'MarkerSize',markersize);
        plot(i2,j2,'o','MarkerFaceColor',[0.23,0.1,0.76],'MarkerEdgeColor',[0.23,0.1,0.76],'MarkerSize',markersize);
        plot(i3,j3,'o','MarkerFaceColor',[0.86,0.71,0.09],'MarkerEdgeColor',[0.86,0.71,0.09],'MarkerSize',markersize);
        plot(i4,j4,'o','MarkerFaceColor',[1,1,1],'MarkerEdgeColor',[1,1,1],'MarkerSize',markersize);
        %         hold on;
        pause(.1)
        
        set(gcf,'position',[0,0,Lsize,Lsize],title(gca,sprintf('%d',ii)))
        frame = getframe(gcf);
        writeVideo(myVideo, frame);
        dlmwrite(sprintf('stack_intra%d.csv',mobility),stack_intra,'delimiter',',','-append');
    end
end
figure;
plot(pre+1:ii,sur(:,pre+1:ii));
save(sprintf('file_stack_2intra%d',mobility),'stack_intra');
% close(myVideo)
end
