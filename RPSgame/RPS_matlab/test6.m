clear all; close all; clc

bio

function bio

Lsize=50;

L=randi([1,3],Lsize,Lsize);
RR=sprand(Lsize,Lsize,0.05)+0==0;
L=L.*RR;
B=zeros(Lsize+2,1);
C=zeros(1,Lsize);
AC=[C;L;C];
Lattice=[B,AC,B];

pre=0;
ite=3500;

M=3e-05
reproduction_rate=1
selection_rate=1
eps=M*(Lsize^2)*(1/2)
figure;
r1=(reproduction_rate)/(reproduction_rate+selection_rate+eps);
r2=(selection_rate)/(reproduction_rate+selection_rate+eps);
r3=(eps)/(reproduction_rate+selection_rate+eps);
myVideo = VideoWriter('a6.avi');
myVideo.FrameRate = 100;
open(myVideo)
for ii=1:pre+ite
    for i = 1:Lsize^2
        R=randi([2,Lsize+1],1,2);
        rr=randi(4);
        switch rr
            case 1
                A=[1,0];
            case 2
                A=[-1,0];
            case 3
                A=[0,1];
            case 4
                A=[0,-1];
        end
        neighbor=Lattice(R(1)+A(1),R(2)+A(2)); main=Lattice(R(1),R(2));
        p=rand;
        if p < r3
            Lattice(R(1)+A(1),R(2)+A(2))=main;
            Lattice(R(1),R(2))=neighbor;
        elseif p >= r3 && p < r3+r1
            if neighbor==0 && main~=0
                Lattice(R(1)+A(1),R(2)+A(2))=main;
            end
        else
            if neighbor~=0 && main~=0
                if neighbor-main==1
                    Lattice(R(1)+A(1),R(2)+A(2))=0;
                elseif neighbor-main==-2
                    Lattice(R(1)+A(1),R(2)+A(2))=0;
                end
            end
        end
    end

    if mod(ii,6)==1
        isOne=Lattice==1;
        isTwo=Lattice==2;
        isThree=Lattice==3;
        isZero=Lattice==0;
        [i1,j1]=ind2sub(size(Lattice),find(isOne));
        [i2,j2]=ind2sub(size(Lattice),find(isTwo));
        [i3,j3]=ind2sub(size(Lattice),find(isThree));
        [i4,j4]=ind2sub(size(Lattice),find(isZero));
        axes('NextPlot','add','XLim',[1,Lsize],'YLim',[1,Lsize]);
        axis square;
        plot(i1,j1,'o','MarkerFaceColor',[0.84,0.18,0.18],'MarkerEdgeColor',[0.84,0.18,0.18],'MarkerSize',0.7);
        plot(i2,j2,'o','MarkerFaceColor',[0.23,0.1,0.76],'MarkerEdgeColor',[0.23,0.1,0.76],'MarkerSize',0.7);
        plot(i3,j3,'o','MarkerFaceColor',[0.86,0.71,0.09],'MarkerEdgeColor',[0.86,0.71,0.09],'MarkerSize',0.7);
        plot(i4,j4,'o','MarkerFaceColor',[0.07,0.07,0.07],'MarkerEdgeColor',[0.07,0.07,0.07],'MarkerSize',0.3);
%         hold on;
        pause(.01)
        
        set(gcf,'position',[0,0,Lsize,Lsize])
        frame = getframe(gcf);
        writeVideo(myVideo, frame);
    end
        
end
close(myVideo)
end

