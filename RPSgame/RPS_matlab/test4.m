close all; clear all; clc

Lsize=1000;

A=randi([1,4],Lsize,Lsize);
% A=4*ones(Lsize,Lsize);
% A(randi([1,Lsize]),randi(1,Lsize))=1;
% A(randi([1,Lsize]),randi(1,Lsize))=2;
% A(randi([1,Lsize]),randi(1,Lsize))=3;
B=4*ones(Lsize+2,1);
C=4*ones(1,Lsize);
AC=[C;A;C];
Lattice=[B,AC,B];
mobile_probability=0.5
pre=500;
ite=50;
M=2*mobile_probability*((Lsize*Lsize)^(-1))
figure;

myVideo = VideoWriter('a1.avi');
myVideo.FrameRate = 50;
open(myVideo)
for ii=1:pre+ite
    for i = 2:Lsize+1
        for j =2:Lsize+1
            A=move;
            neighbor=Lattice(i+A(1),j+A(2)); main=Lattice(i,j);
            p=rand;
            if p < mobile_probability
                Lattice(i+A(1),j+A(2))=main;
                Lattice(i,j)=neighbor;
            elseif p >= mobile_probability
                if neighbor==4 && main~=4
                    q=rand;
                    if q <= 0.01
                        Lattice(i+A(1),j+A(2))=main;
                    end
                elseif neighbor~=4 && main~=4
                    r=rand;
                    if r <= 0.01
                        if neighbor-main==1
                            Lattice(i+A(1),j+A(2))=4;
                        elseif neighbor-main==-2
                            Lattice(i+A(1),j+A(2))=4;
                        end
                    end
                end
            end
        end
    end
    if ii > pre
        isOne=Lattice==1;
        isTwo=Lattice==2;
        isThree=Lattice==3;
        [i1,j1]=ind2sub(size(Lattice),find(isOne));
        [i2,j2]=ind2sub(size(Lattice),find(isTwo));
        [i3,j3]=ind2sub(size(Lattice),find(isThree));
        axes('NextPlot','add','XLim',[1,Lsize],'YLim',[1,Lsize]);
        axis square;
        plot(i1,j1,'o','MarkerFaceColor',[0.84,0.18,0.18],'MarkerEdgeColor',[0.84,0.18,0.18],'MarkerSize',0.1);
        plot(i2,j2,'o','MarkerFaceColor',[0.23,0.1,0.76],'MarkerEdgeColor',[0.23,0.1,0.76],'MarkerSize',0.1);
        plot(i3,j3,'o','MarkerFaceColor',[0.86,0.71,0.09],'MarkerEdgeColor',[0.86,0.71,0.09],'MarkerSize',0.1);
        hold on;
        pause(.01)
        
        frame = getframe(gcf);
        writeVideo(myVideo, frame);
    end
    
end
close(myVideo)
% isOne=Lattice==1;
% isTwo=Lattice==2;
% isThree=Lattice==3;
% [i1,j1]=ind2sub(size(Lattice),find(isOne));
% [i2,j2]=ind2sub(size(Lattice),find(isTwo));
% [i3,j3]=ind2sub(size(Lattice),find(isThree));
% axes('NextPlot','add','XLim',[1,Lsize],'YLim',[1,Lsize]);
% axis square;
% plot(i1,j1,'o','MarkerFaceColor',[0.84,0.18,0.18],'MarkerEdgeColor',[0.84,0.18,0.18],'MarkerSize',3);
% plot(i2,j2,'o','MarkerFaceColor',[0.23,0.1,0.76],'MarkerEdgeColor',[0.23,0.1,0.76],'MarkerSize',3);
% plot(i3,j3,'o','MarkerFaceColor',[0.86,0.71,0.09],'MarkerEdgeColor',[0.86,0.71,0.09],'MarkerSize',3);
