using SparseArrays, Plots, JLD, Printf, Base.Threads, DelimitedFiles, TickTock
function RPS_inter2(Lsize, pre, ite, reproduction_rate, selection_rate, mobility)
    tick()
    # Lsize=10;
    # pre=0;
    # ite=10;
    # mobility=50;
    # reproduction_rate=1;
    # selection_rate=1;
    
    #spreda species in lattice randomly
    S = rand(1:3, Lsize, Lsize);
    SS = sprand(Lsize, Lsize, 0.9) .>0;
    Lattice = S .* SS ;

    #tracking lifespan of elements in lattice
    Trace = ones(Lsize, Lsize) .* SS ;
    T_Trace = zeros(Lsize,Lsize);
    # s_Lattice = zeros(Lsize, Lsize, pre + ite);

    M = 5*10^(-mobility*(1/10));
    eps = M * (Lsize^2) * (1 / 2);

    r1 = (reproduction_rate) / (reproduction_rate + selection_rate + eps);
    r2 = (selection_rate) / (reproduction_rate + selection_rate + eps);
    r3 = (eps) / (reproduction_rate + selection_rate + eps);

    L=[[1,0] [-1,0] [0,1] [0,-1]];

    stack=Vector{Float64}();
    #a generation
    for ii in 1 : pre + ite
        #a step(interation)
        c=rand(1:4,Lsize^2,1);#choose a neighbor of chosen elements
        R=rand(1:Lsize,Lsize^2,2);
        p=rand(Lsize^2,1);
        for i in 1:Lsize^2
                        
            N1 = R[i,1] + L[1,c[i]]; N2 = R[i,2] + L[2,c[i]];

            #periodic boundary condition
            if N1 > Lsize
                N1 = 1;
            elseif N2 > Lsize
                N2 = 1;
            elseif N1 < 1
                N1 = Lsize;
            elseif N2 < 1
                N2 = Lsize;
            end

            neighbor = Lattice[N1,N2];
            host = Lattice[R[i,1],R[i,2]];
            n_trace = Trace[N1,N2];
            h_trace = Trace[R[i,1],R[i,2]];
            nn_trace = T_Trace[N1,N2];
            nh_trace = T_Trace[R[i,1],R[i,2]];
            p=rand();
            if p < r3 #move
                Lattice[N1,N2] = host;
                Lattice[R[i,1],R[i,2]] = neighbor;
                Trace[N1,N2] = h_trace;
                Trace[R[i,1],R[i,2]] = n_trace;
                T_Trace[N1,N2] = nh_trace;
                T_Trace[R[i,1],R[i,2]] =nn_trace;
            elseif p < r1 + r3 #reproduce
                if neighbor == 0 && host !== 0
                    Lattice[N1,N2] = host;
                    Trace[N1,N2] = 1;
                    T_Trace[N1,N2] = 1;
                end
            elseif p < r1 + r2 + r3 #selection
                if neighbor !== 0 && host !== 0
                    if neighbor - host == 1
                        Lattice[N1,N2] = 0;
                        append!(stack, Trace[N1,N2]);
                        Trace[N1,N2] = 0;
                        T_Trace[N1,N2] = 0;
                    elseif neighbor - host == -2
                        Lattice[N1,N2] = 0;
                        append!(stack, Trace[N1,N2]);
                        Trace[N1,N2] = 0;
                        T_Trace[N1,N2] = 0;
                    end
                end
            end

        end
        Trace = Trace + (Trace .> 0) - T_Trace;
        T_Trace = zeros(Lsize, Lsize);
        # if ii > pre
        #     s_Lattice[:,:,ii] = Lattice;
        # end

    end

    # anim = @animate for k = 1 + pre:pre + ite
    #     heatmap(s_Lattice[:,:,k], color=:Blues)
    # end

###MAC
    # numb=string(mobility);
    # file1 = @sprintf "/Users/tyoon/Desktop/Git/rps/RPSgame/RPS_ani_inter%s.avi" numb
    # file2 = @sprintf "/Users/tyoon/Desktop/Git/rps/RPSgame/stack%s.jld" numb
    # gif(anim,file1, fps=100)
    # save(file2,"stack",stack)
###Window
    # numb=string(mobility);
    # file1 = @sprintf "/Users/Kcore/YOONGIT/rps/RPSgame/RPS_ani_inter%s.avi" numb
    # file2 = @sprintf "/Users/Kcore/YOONGIT/rps/RPSgame/Rps_julia/stack_inter%s.csv" numb
    # gif(anim,file1, fps=25)
    # save(file2,"stack",stack)
    # io=open("append.csv", "a")
    # writedlm(io, stack)
    # close(io)
    writedlm("append.csv",stack,',')
    tock()
end
# tick()
# RPS_inter(500,100000,0,34,1,1)
# tock()
# tick()
# Threads.@threads for i in 1:1
#     M=(i-1)*2+30
#     RPS_inter2(500,300,0,1,1,M);
# end
# tock()