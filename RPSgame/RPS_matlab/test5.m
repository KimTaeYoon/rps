clear all; close all; clc

bio

function bio

Lsize=1000;

A=randi([1,4],Lsize,Lsize);
% A=4*ones(Lsize,Lsize);
% A(randi([1,Lsize]),randi(1,Lsize))=1;
% A(randi([1,Lsize]),randi(1,Lsize))=2;
% A(randi([1,Lsize]),randi(1,Lsize))=3;
B=4*ones(Lsize+2,1);
C=4*ones(1,Lsize);
AC=[C;A;C];
Lattice=[B,AC,B];
pre=5000;
ite=500;
M=1e-05
eps=M*(Lsize^2)*(1/2)
figure;
r1=(1)/(1+1+eps)
r2=(1)/(1+1+eps)
r3=(eps)/(1+1+eps)
myVideo = VideoWriter('a2.avi');
myVideo.FrameRate = 100;
open(myVideo)
for ii=1:pre+ite
    for i = 1:Lsize^2
        R=randi([2,Lsize+1],1,2);
        A=move;
        neighbor=Lattice(R(1)+A(1),R(2)+A(2)); main=Lattice(R(1),R(2));
        p=rand;
        if p < r3
            Lattice(R(1)+A(1),R(2)+A(2))=main;
            Lattice(R(1),R(2))=neighbor;
        elseif p >= r3
            if neighbor==4 && main~=4
                q=rand;
                if q <= r1
                    Lattice(R(1)+A(1),R(2)+A(2))=main;
                end
            elseif neighbor~=4 && main~=4
                r=rand;
                if r <= r2
                    if neighbor-main==1
                        Lattice(R(1)+A(1),R(2)+A(2))=4;
                    elseif neighbor-main==-2
                        Lattice(R(1)+A(1),R(2)+A(2))=4;
                    end
                end
            end
        end
    end
        if ii > pre
            isOne=Lattice==1;
            isTwo=Lattice==2;
            isThree=Lattice==3;
            isFour=Latice==4;
            [i1,j1]=ind2sub(size(Lattice),find(isOne));
            [i2,j2]=ind2sub(size(Lattice),find(isTwo));
            [i3,j3]=ind2sub(size(Lattice),find(isThree));
            [i4,j4]=ind2sub(size(Lattice),find(isFour));
            axes('NextPlot','add','XLim',[1,Lsize],'YLim',[1,Lsize]);
            axis square;
            plot(i1,j1,'o','MarkerFaceColor',[0.84,0.18,0.18],'MarkerEdgeColor',[0.84,0.18,0.18],'MarkerSize',0.2);
            plot(i2,j2,'o','MarkerFaceColor',[0.23,0.1,0.76],'MarkerEdgeColor',[0.23,0.1,0.76],'MarkerSize',0.2);
            plot(i3,j3,'o','MarkerFaceColor',[0.86,0.71,0.09],'MarkerEdgeColor',[0.86,0.71,0.09],'MarkerSize',0.2);
            plot(i4,j4,'o','MarkerFaceColor',[0.9,0.9,0.9],'MarkerEdgeColor',[0.9,0.9,0.9],'MarkerSize',0.2);
            hold on;
            pause(.01)
    
            frame = getframe(gcf);
            writeVideo(myVideo, frame);
        end
    
end
close(myVideo)
% isOne=Lattice==1;
% isTwo=Lattice==2;
% isThree=Lattice==3;
% [i1,j1]=ind2sub(size(Lattice),find(isOne));
% [i2,j2]=ind2sub(size(Lattice),find(isTwo));
% [i3,j3]=ind2sub(size(Lattice),find(isThree));
% axes('NextPlot','add','XLim',[1,Lsize],'YLim',[1,Lsize]);
% axis square;
% plot(i1,j1,'o','MarkerFaceColor',[0.84,0.18,0.18],'MarkerEdgeColor',[0.84,0.18,0.18],'MarkerSize',0.5)
% plot(i2,j2,'o','MarkerFaceColor',[0.23,0.1,0.76],'MarkerEdgeColor',[0.23,0.1,0.76],'MarkerSize',0.5)
% plot(i3,j3,'o','MarkerFaceColor',[0.86,0.71,0.09],'MarkerEdgeColor',[0.86,0.71,0.09],'MarkerSize',0.5)
end



function L=move
l=[-1,0,1];
a=l(randi([1,3],1));
if a == 0
    ll=l(l~=a);
    b=ll(randi([1,2],1));
    L=[a,b];
else
    b=0;
    L=[a,b];
end
end
