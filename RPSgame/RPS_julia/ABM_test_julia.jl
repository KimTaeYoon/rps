using SparseArrays
using Plots
using TickTock
using Colors
Lsize=5;

x=rand(1:Lsize,Lsize,1);
y=rand(1:Lsize,Lsize,1);

anim = @animate for i = 1:100
    move=2*rand(5,2).-1;
    x=mod.(x+move[:,1],Lsize); y=mod.(y+move[:,2],Lsize);
    plot(x,y,xlims=(0,Lsize),ylims=(0,Lsize), seriestype =:scatter, c=colormap("RdBu",5), title="test")
end
gif(anim,"test.gif",fps=1)