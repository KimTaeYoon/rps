using SparseArrays, Plots, JLD, Printf, Base.Threads, DelimitedFiles, TickTock

function RPS_intra(Lsize, pre, ite, mobility, reproduction_rate, selection_rate, intra1, intra2, intra3)

    S = rand(1:3, Lsize, Lsize);
    SS = sprand(Lsize, Lsize, 0.9) .> 0;
    Lattice = S .* SS;

    Trace = zeros(Lsize, Lsize, 2);
    Trace[:,:,1] = ones(Lsize, Lsize) .* SS;

    stack = Vector{Float64}();
    # s_Lattice = zeros(Lsize, Lsize, pre + ite);

    M = 5*10^(-mobility*(1/10));
    eps = M * (Lsize^2) * (1 / 2);

    intra_sum=intra1+intra2+intra3;

    r1 = (reproduction_rate) / (reproduction_rate + selection_rate + eps + intra_sum);
    r2 = (selection_rate) / (reproduction_rate + selection_rate + eps + intra_sum);
    r3 = (eps) / (reproduction_rate + selection_rate + eps + intra_sum);
    r4 = (intra1) / (reproduction_rate + selection_rate + eps + intra_sum);
    r5 = (intra2) / (reproduction_rate + selection_rate + eps + intra_sum);
    r6 = (intra3) / (reproduction_rate + selection_rate + eps + intra_sum);

    for ii in 1:pre + ite

        for i in 1:Lsize^2

            R = rand(1:Lsize, 1, 2);
            c = rand(1:4);


            if c == 1
                L = [1,0];
            elseif c == 2
                L = [-1,0];
            elseif c == 3
                 L = [0,1];
            else
                L = [0,-1];
            end

            N1 = R[1] + L[1]; N2 = R[2] + L[2];

            if N1 > Lsize
                N1 = 1;
            elseif N2 > Lsize
                N2 = 1;
            elseif N1 < 1
                N1 = Lsize;
            elseif N2 < 1
                N2 = Lsize;
            end
            neighbor = Lattice[N1,N2];
            host = Lattice[R[1],R[2]];
            n_trace = Trace[N1,N2,:];
            h_trace = Trace[R[1],R[2],:];
            p = rand();

            if p < r3
                Lattice[N1,N2] = host;
                Lattice[R[1],R[2]] = neighbor;
                Trace[N1,N2,:] = h_trace;
                Trace[R[1],R[2],:] .= n_trace;
            elseif p < r1 + r3
                if neighbor == 0 && host !== 0
                    Lattice[N1,N2] = host;
                    Trace[N1,N2,:] .= 1;
                end
            elseif p < r1 + r2 + r3 + r4 + r5 + r6
                if neighbor !== 0 && host !== 0
                    if p < r1 + r2 + r3
                        if neighbor - host == 1
                            Lattice[N1,N2] = 0;
                            append!(stack, Trace[N1,N2,1]);
                            Trace[N1,N2,:] .= 0;
                        elseif neighbor - host == -2
                            Lattice[N1,N2] = 0;
                            append!(stack, Trace[N1,N2,1]);
                            Trace[N1,N2,:] .= 0;
                        end
                    elseif p < r1 + r2 + r3 + r4
                        if host == 1 && neighbor == 1
                            Lattice[N1,N2] = 0;
                            append!(stack, Trace[N1,N2,1]);
                            Trace[N1,N2,:] .= 0;
                        end
                    elseif p < r1 + r2 + r3 + r4 + r5
                        if host == 2 && neighbor == 2
                            Lattice[N1,N2] = 0;
                            append!(stack, Trace[N1,N2,1]);
                            Trace[N1,N2,:] .= 0;
                        end
                    elseif p < r1 + r2 + r3 + r4 + r5 + r6
                        if host == 3 && neighbor == 3
                            Lattice[N1,N2] = 0;
                            append!(stack, Trace[N1,N2,1]);
                            Trace[N1,N2,:] .= 0;
                        end
                    end

                end

            end
        end

        Trace[:,:,1] = Trace[:,:,1] + (Trace[:,:,1] .> 0.00001) - Trace[:,:,2];
        Trace[:,:,2] = zeros(Lsize, Lsize);
        # if ii > pre
        #     s_Lattice[:,:,ii] = Lattice;
        # end
    end

    # anim = @animate for k = 1 + pre:pre + ite
    #     heatmap(s_Lattice[:,:,k], color=:Set1_4)
    # end

###MAC
    # numb=string(mobility);
    # file1 = @sprintf "/Users/tyoon/Desktop/Git/rps/RPSgame/RPS_ani_inter%s.avi" numb
    # file2 = @sprintf "/Users/tyoon/Desktop/Git/rps/RPSgame/stack%s.jld" numb
    # gif(anim,file1, fps=100)
    # save(file2,"stack",stack)
###Window
    numb=string(mobility);
    # file1 = @sprintf "/Users/Kcore/YOONGIT/rps/RPSgame/RPS_ani_intra%s.avi" numb
    file2 = @sprintf "/Users/Kcore/YOONGIT/rps/RPSgame/RPS_julia/stack_intra%s.csv" numb
    # gif(anim,file1, fps=100)
    writedlm(file2,stack,',')
###Dell
    #numb=string(mobility);
    #file1 = @sprintf "/Users/KTY/YOONGIT/rps/RPSgame/RPS_ani_intra%s.mp4" numb
    #file2 = @sprintf "/Users/KTY/YOONGIT/rps/RPSgame/stack_intra%s.jld" numb
    #gif(anim,file1, fps=10)
    #save(file2,"stack",stack)
end
tick()
Threads.@threads for i in 1:16
    M=(i-1)*2+30;
    RPS_intra(500,50000,0,M,1,1,2.5,1,0.5);
end
tock()
